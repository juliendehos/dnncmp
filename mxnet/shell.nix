let 

  mkPkgsWithCuda = withCuda: import <nixpkgs> { 
    config = {
      allowUnfree = withCuda;
      cudaSupport = withCuda;
      cudnnSupport = withCuda;
    };
  };

  mkEnvWithCuda = withCuda: ((mkPkgsWithCuda withCuda).python3.withPackages(ps: [ ps.mxnet ])).env;

in

  {
    cpu = mkEnvWithCuda false;
    gpu = mkEnvWithCuda true;
  }


# nix run "(with import <nixpkgs> { config = { allowUnfree=true; cudaSupport=true; cudnnSupport=true; }; }; python3.withPackages (ps: [ ps.mxnet ]))"

# nix run "(with import <nixpkgs> {}; python3.withPackages (ps: [ ps.mxnet ]))"


