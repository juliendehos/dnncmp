using Knet

# Define convolutional layer:
struct Conv; w; b; f; end
(c::Conv)(x) = c.f.(pool(conv4(c.w, x) .+ c.b))
Conv(w1,w2,cx,cy,f=relu) = Conv(param(w1,w2,cx,cy), param0(1,1,cy,1), f)

# Define dense layer:
struct Dense; w; b; f; end
(d::Dense)(x) = d.f.(d.w * mat(x) .+ d.b)
Dense(i::Int,o::Int,f=relu) = Dense(param(o,i), param0(o), f)

# Define a chain of layers:
struct Chain; layers; end
(c::Chain)(x) = (for l in c.layers; x = l(x); end; x)

# Define the LeNet model
LeNet = Chain((Conv(5,5,1,20), Conv(5,5,20,50), Dense(800,500), Dense(500,10,identity)))

# Train and test LeNet (about 30 secs on a gpu to reach 99% accuracy)
println("loading data...")
include(Knet.dir("data","mnist.jl"))
dtrn, dtst = mnistdata()
println("training...")
@time train!(LeNet, dtrn)
@show accuracy(LeNet, dtst)

