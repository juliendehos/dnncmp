let

  # _channel = fetchTarball "https://github.com/NixOS/nixpkgs/archive/18.09.tar.gz";
  _channel = <nixpkgs>;

  mkPkgsWithCuda = opt : import _channel { 
    config = {
      allowUnfree = opt;
      cudaSupport = opt;
    };
  };

  mkEnv = pkgs: pkgs.mkShell;
  
in

{

  cpu = let pkgs = mkPkgsWithCuda false; in mkEnv pkgs {
    shellHook = ''
      export LD_LIBRARY_PATH="$LD_LIBRARY_PATH:${pkgs.zlib}/lib";
    '';
  };

  gpu = let pkgs = mkPkgsWithCuda true; in mkEnv pkgs {
     buildInput = [
       pkgs.cudnn
     ];
     shellHook = ''
       export CUDA_PATH="${pkgs.cudatoolkit}"  
       export LD_LIBRARY_PATH="$LD_LIBRARY_PATH:${pkgs.zlib}/lib:${pkgs.cudatoolkit}/lib:${pkgs.cudnn}/lib";
     '';
  };

}

