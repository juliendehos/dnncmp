let
  config = {
    allowUnfree = true;
    cudaSupport = true;
  };
  channel = fetchTarball "https://github.com/NixOS/nixpkgs/archive/35e19d2.tar.gz";
  #channel = <nixpkgs>;
  pkgs = import channel { inherit config; };
  #pkgs = import channel { };
  #_julia = (pkgs.julia.overrideDerivation (attrs: { doCheck = false; }));
  #pytorch = pkgs.python3Packages.pytorchWithoutCuda;
  pytorch = pkgs.python3Packages.pytorchWithCuda;
  python = pkgs.python3;

in with pkgs; mkShell {
  buildInputs = [
    cmake
    gnumake
    gcc
    pytorch
    cudnn
    cudatoolkit
  ];
  
  shellHook = ''
  #     export CUDA_PATH="${cudatoolkit}"  
      export CFLAGS="-I${pybind11}/include -I${pytorch}/${python.sitePackages}/torch/include -I${pytorch}/${python.sitePackages}/torch/include/torch/csrc/api/include"
      export CXXFLAGS=$CFLAGS
      export LDFLAGS="-L${pytorch}/${python.sitePackages}/torch/lib -L$out/${python.sitePackages}:${pkgs.cudatoolkit}/lib:${pkgs.cudnn}/lib:${pkgs.linuxPackages.nvidia_x11}/lib"
      export OMP_NUM_THREADS=1
  '';

      #export LDFLAGS="-L${pytorch}/${python.sitePackages}/torch/lib -L$out/${python.sitePackages}"
      #export LDFLAGS="-L${pytorch}/${python.sitePackages}/torch/lib -L$out/${python.sitePackages}:${pkgs.cudatoolkit}/lib:${pkgs.cudnn}/lib:${pkgs.linuxPackages.nvidia_x11}/lib"

  #shellHook = ''
  #     export CUDA_PATH="${pkgs.cudatoolkit}"  
  #     export LD_LIBRARY_PATH="$LD_LIBRARY_PATH:${pkgs.zlib}/lib:${pkgs.cudatoolkit}/lib:${pkgs.cudnn}/lib:${pkgs.linuxPackages.nvidia_x11}/lib:${pkgs.stdenv.cc.cc.lib}/lib/"
  #'';
}

