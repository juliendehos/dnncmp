let
  config = {
    allowUnfree = true;
    cudaSupport = false;
  };
  channel = fetchTarball "https://github.com/NixOS/nixpkgs/archive/35e19d2.tar.gz";
  pkgs = import channel { inherit config; };
  #_julia = (pkgs.julia.overrideDerivation (attrs: { doCheck = false; }));

# stdenv, cmake, libtorch-bin, symlinkJoin }:

in with pkgs; stdenv.mkDerivation {
  pname = "libtorch-t1";
  version = libtorch-bin.version;

  src = ./.;

  postPatch = ''
    cat CMakeLists.txt
  '';

  makeFlags = [ "VERBOSE=1" ];

  nativeBuildInputs = [ cmake ];

  buildInputs = [ libtorch-bin ];

  installPhase = ''
    touch $out
  '';

  checkPhase = ''
    ./t1
  '';
}
