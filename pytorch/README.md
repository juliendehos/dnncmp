
## manual

```
nix-shell shell-all.nix -A cpu.env --run "python mnist.py"
nix-shell shell-all.nix -A cuda.env --run "python mnist.py"
```

## nix-direnv

`shell.nix`:

```
(import ./shell-all.nix).cuda.env
```

```
echo "use nix" > .envrc
direnv allow
```

## todo

```
/etc/nixos/configuration.nix <- nix.trustedUsers = [ "root" "@wheel" ];

nix --option sandbox false run "(with import <nixpkgs> { config = { allowUnfree = true; cudaSupport = true; }; }; python3.withPackages (ps: [ ps.pytorchWithCuda ps.torchvision ]))"
```

