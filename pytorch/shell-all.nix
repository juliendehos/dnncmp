with import <nixpkgs> {};

{

  cpu = python3.withPackages (ps: with ps; [
    pytorch
    torchvision
  ]);

  cuda = python3.withPackages (ps: with ps; [
    cudnn
    pytorchWithCuda
    torchvision
  ]);

}

