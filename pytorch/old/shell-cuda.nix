let

  rev = "dfa8e8b9bc4a18bab8f2c55897b6054d31e2c71b";
  channel = fetchTarball "https://github.com/NixOS/nixpkgs/archive/${rev}.tar.gz";
  config = {
    allowUnfree = true;
    cudaSupport = true;
    packageOverrides = pkgs: {
      cudatoolkit = pkgs.cudatoolkit_10;
      cudnn = pkgs.cudnn_cudatoolkit_10;
    };
  };

  pkgs = import channel { inherit config; };
  python = pkgs.python3;

  tensorboardX = python.pkgs.buildPythonPackage rec {
    pname = "tensorboardX";
    version = "1.8";
    src = fetchTarball "https://github.com/lanpa/tensorboardX/archive/v1.8.tar.gz";
    propagatedBuildInputs = with pkgs.python3Packages; [
      six
      protobuf
      numpy
    ];
    doCheck = false;
  };

in pkgs.mkShell {

  buildInputs = [
    pkgs.cudatoolkit
    pkgs.cudnn
    pkgs.linuxPackages.nvidia_x11

    python.pkgs.pytorchWithCuda
    python.pkgs.torchvision
    tensorboardX
  ];

}

