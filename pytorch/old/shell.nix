let 

  #_rev = "5765be2090976d971528aeeafdd36a837f4a9946";
  #_rev = "8c645b4b8a54f902137e702f6f5f24c969c3505a";
  #_url = "https://github.com/NixOS/nixpkgs/archive/${_rev}.tar.gz";
  #_channel = fetchTarball _url;
  _channel = <nixpkgs>;

  mkPkgsWithCuda = opt : import _channel { 
    config = {
      allowUnfree = opt;
      cudaSupport = opt;
    };
  };

  mkEnv = pkgs: mkPs: (pkgs.python3.withPackages(mkPs)).env;

in

  {

    cpu = mkEnv (mkPkgsWithCuda false) (ps: [ 
      ps.pytorch
      ps.torchvision
    ]);

    gpu = let pkgs = mkPkgsWithCuda true; in mkEnv pkgs (ps: [ 

      # ps.pytorchWithCuda.override {
      #   cudatoolkit = pkgs.cudatoolkit_10;
      #   cudnn = pkgs.cudnn_cudatoolkit_10;
      # }

      ps.pytorchWithCuda
      pkgs.cudnn

      ps.torchvision
    ]);

  }

