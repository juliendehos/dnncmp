import tensorflow as tf
mnist = tf.keras.datasets.mnist

(x_train, y_train), (x_test, y_test) = mnist.load_data()
x_train, x_test = x_train / 255.0, x_test / 255.0

num_train, img_rows, img_cols = x_train.shape
depth = 1
input_shape = (img_rows, img_cols, depth)

x_train = x_train.reshape(x_train.shape[0], img_rows, img_cols, depth)
x_test = x_test.reshape(x_test.shape[0], img_rows, img_cols, depth)

model = tf.keras.models.Sequential([
    tf.keras.layers.Conv2D(32, kernel_size=(3, 3), activation='relu',
                           input_shape=input_shape),
    tf.keras.layers.MaxPooling2D(pool_size=(2, 2)),
    tf.keras.layers.Flatten(),
    tf.keras.layers.Dropout(0.5),
    tf.keras.layers.Dense(10, activation=tf.nn.softmax)])

model.compile(optimizer='adam', loss='sparse_categorical_crossentropy',
              metrics=['accuracy'])

model.fit(x_train, y_train, epochs=6)
print(model.evaluate(x_test, y_test))
