
cpu:

```
nix-shell --run "time python mnist.py" -A cpu 
```


gpu:

```
nix-shell --run "time python mnist.py" -A gpu 
```

