let 

  _channel = fetchTarball "https://github.com/NixOS/nixpkgs/archive/18.09.tar.gz";
  # _channel = <nixpkgs>;

  _pkgs_cpu = import _channel { 
    config = {
      allowUnfree = false;
      cudaSupport = false;
    };
  };

  _pkgs_gpu = import _channel { 
    config = {
      allowUnfree = true;
      cudaSupport = true;
    };
  };

in

  {

    cpu = (_pkgs_cpu.python3.withPackages( ps: [ 
      ps.tensorflow
    ])).env;

    gpu = (_pkgs_gpu.python3.withPackages( ps: [ 
      _pkgs_gpu.cudnn
      ps.tensorflowWithCuda
    ])).env;

  }

