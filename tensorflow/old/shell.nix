let 

  #_channel = fetchTarball "https://github.com/NixOS/nixpkgs/archive/18.09.tar.gz";
   _channel = <nixpkgs>;

  mkPkgsWithCuda = opt: import _channel { 
    config = {
      allowUnfree = opt;
      cudaSupport = opt;
    };
  };

  mkEnv = pkgs: mkPs: (pkgs.python36.withPackages(mkPs)).env;

in

  {

    cpu = mkEnv (mkPkgsWithCuda false) (ps: [ 
      ps.tensorflow
    ]);

    gpu = let pkgs = mkPkgsWithCuda true; in mkEnv pkgs (ps: [ 
      pkgs.cudnn
      ps.tensorflowWithCuda
    ]);

  }

