
let

    config = {
      allowUnfree = true;
      cudaSupport = true;
    };

  pkgs = import <nixpkgs> { inherit config; };

  _julia = pkgs.julia;
  #_julia = pkgs.callPackage ./julia.nix {};

in

  with pkgs;

  mkShell {
    buildInputs = [
      gnome3.eog
      cudnn
      cudatoolkit
      _julia 
    ];

     shellHook = ''
       export CUDA_PATH="${pkgs.cudatoolkit}"  
       export LD_LIBRARY_PATH="$LD_LIBRARY_PATH:${pkgs.zlib}/lib:${pkgs.cudatoolkit}/lib:${pkgs.cudnn}/lib:${pkgs.linuxPackages.nvidia_x11}/lib:${stdenv.cc.cc.lib}/lib/"
     '';

    SSL_CERT_FILE="/etc/ssl/certs/ca-certificates.crt";
    GIT_SSL_CAINFO="/etc/ssl/certs/ca-certificates.crt";

  }

