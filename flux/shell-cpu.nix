with import <nixpkgs> {};

mkShell {
  buildInputs = [
    gnome3.eog
    python3
    python3Packages.matplotlib
    python3Packages.tkinter
    julia 
  ];

  SSL_CERT_FILE="/etc/ssl/certs/ca-certificates.crt";
  GIT_SSL_CAINFO="/etc/ssl/certs/ca-certificates.crt";

}

