let

  #_rev = "5765be2090976d971528aeeafdd36a837f4a9946";
  #_url = "https://github.com/NixOS/nixpkgs/archive/${_rev}.tar.gz";
  #_channel = fetchTarball _url;
  _channel = <nixpkgs>;

  mkPkgsWithCuda = opt : import _channel { 
    config = {
      allowUnfree = opt;
      cudaSupport = opt;
    };
  };

  mkEnv = pkgs: pkgs.mkShell;

in

{

  cpu = let pkgs = mkPkgsWithCuda false; in mkEnv pkgs {
    buildInput = [
      (pkgs.callPackage ./julia.nix {})
    ];
    shellHook = ''
      export LD_LIBRARY_PATH="$LD_LIBRARY_PATH:${pkgs.zlib}/lib";
    '';
  };

  gpu = let pkgs = mkPkgsWithCuda true; in mkEnv pkgs {
     buildInput = [
      (pkgs.callPackage ./julia.nix {})
       pkgs.cudnn
       pkgs.cudatoolkit
       #pkgs.linuxPackages.nvidia_x11
     ];
     shellHook = ''
       export CUDA_PATH="${pkgs.cudatoolkit}"  
       export LD_LIBRARY_PATH="$LD_LIBRARY_PATH:${pkgs.zlib}/lib:${pkgs.cudatoolkit}/lib:${pkgs.cudnn}/lib";
     '';
  };

}

