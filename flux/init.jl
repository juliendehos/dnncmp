import Pkg

Pkg.add("CUDA")
Pkg.add("CUDAapi")
Pkg.add("BSON")
Pkg.add("Parameters")

Pkg.add("CuArrays")
using CuArrays

Pkg.add("Flux")
using Flux

