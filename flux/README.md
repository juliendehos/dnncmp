
cpu :

```
nix-shell shell-cpu.nix --run julia 
include("init.jl")
@time include("mnist.jl")
exit()
```


gpu:

```
nix-shell shell-gpu.nix --run julia 
include("init.jl")
using CuArrays
@time include("mnist.jl")
exit()
```

