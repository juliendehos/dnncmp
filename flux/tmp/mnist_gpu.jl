using Flux, Flux.Data.MNIST, Statistics
using Flux: onehotbatch, onecold, crossentropy, throttle, @epochs
using Base.Iterators: repeated, partition
using CuArrays

# load data
imgs = MNIST.images()
labels = onehotbatch(MNIST.labels(), 0:9)

trainX = cat(float.(MNIST.images(:test)[1:1000])..., dims = 4) |> gpu
trainY = onehotbatch(MNIST.labels(:test)[1:1000], 0:9) |> gpu
trainXY = gpu([(cat(float.(imgs[i])..., dims = 4), labels[:,i])
               for i in partition(1:60_000, 1000)])

# create model
m = Chain(
        Conv((3, 3), 1=>32, relu),
        x -> maxpool(x, (2, 2)),
        Dropout(0.25),
        x -> reshape(x, :, size(x, 4)),
        Dropout(0.5),
        Dense(5408, 10),
        softmax) |> gpu

# train
loss(x, y) = crossentropy(m(x), y)
opt = ADAM(params(m))
accuracy(x, y) = mean(onecold(m(x)) .== onecold(y))
evalcb() = @show(accuracy(trainX, trainY))
@epochs 6 Flux.train!(loss, trainXY, opt, cb = throttle(evalcb, 5))

@show m(trainXY[1][1])

