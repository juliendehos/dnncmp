using Flux, Flux.Data.MNIST, Statistics
using Flux: onehotbatch, onecold, crossentropy, throttle, @epochs
using Base.Iterators: repeated
using CuArrays

# load data
imgs = MNIST.images()
labels = MNIST.labels()

# Stack images into one large batch
X = hcat(float.(reshape.(imgs, :))...) |> gpu

# One-hot-encode the labels
Y = onehotbatch(labels, 0:9) |> gpu

# create model
m = Chain(Dense(28^2, 32, relu),
           Dense(32, 10),
           softmax) |> gpu

# train
loss(x, y) = crossentropy(m(x), y)
opt = ADAM(params(m))
accuracy(x, y) = mean(onecold(m(x)) .== onecold(y))
evalcb = () -> @show(loss(X, Y))
dataset = repeated((X, Y), 200)
@epochs 6 Flux.train!(loss, dataset, opt, cb = throttle(evalcb, 10))
accuracy(X, Y)

# test
tX = hcat(float.(reshape.(MNIST.images(:test), :))...) |> gpu
tY = onehotbatch(MNIST.labels(:test), 0:9) |> gpu
accuracy(tX, tY)

