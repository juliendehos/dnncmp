
# Deep Neural Network framework CoMParison

MNIST implemented with tensorflow, flux, pytorch...

**WORK IN PROGRESS !**


## Model

- TODO

```python
model = tf.keras.models.Sequential([
    tf.keras.layers.Conv2D(32, kernel_size=(3, 3), activation='relu', input_shape=input_shape),
    tf.keras.layers.MaxPooling2D(pool_size=(2, 2)),
    tf.keras.layers.Flatten(),
    tf.keras.layers.Dropout(0.5),
    tf.keras.layers.Dense(10, activation=tf.nn.softmax)])

model.compile(optimizer='adam', loss='sparse_categorical_crossentropy', metrics=['accuracy'])

# train using 6 epochs
```


## TODO

- implement MNIST with mxnet 
- flux: multicore cpu, test model
- check DNN parameters


## Benchmarks 1 (NixOS)

### Hardware

- CPU: Intel® Core™ i5-3570K CPU @ 3.40GHz, 4 cores, 16GB
- GPU: NVIDIA GeForce GTX 970, 4GB

### Software

- NVIDIA Driver: 390.87
- tensorflow: 1.9.0
- julia: 1.0.1
- flux: 0.6.7+
- pytorch: 0.4.1
- mxnet: 1.2.1
- NixOS 18.09/19.03-pre

### Measured times 

- tensorflow cpu: 1m32
- tensorflow gpu: 0m36
- pytorch cpu: 3m17
- pytorch gpu: 0m46
- flux cpu: 2m17
- flux gpu: 0m46
- mxnet cpu: TODO
- mxnet gpu: TODO

